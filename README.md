## Derpy brawl

"Derpy Brawl" is a game about Derpy Hooves, built upon Alex McGivern's [squarepig][1] engine.

##### License
Both this game and squarepig are licensed under the [zlib][2] license. Therefore everyone is free to use and modify Derpy Brawl and squarepig.

[1]: https://github.com/aeonofdiscord/squarepig
[2]: http://zlib.net/zlib_license.html
